c = function calculate(divid, fac1, fac2) {
	if (fac2 == null) {
		$("#" + divid).html("");
	} else if (isNaN(fac2)) {
		$("#" + divid).html("<font color=\"red\">fehlerhafte Eingabe</font>");
	} else {
		let prod = fac1 * fac2;
		$("#" + divid).html(prod.toFixed(2));
	}
}
