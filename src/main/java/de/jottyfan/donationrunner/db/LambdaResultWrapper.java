package de.jottyfan.donationrunner.db;

/**
 * 
 * @author jotty
 *
 */
public class LambdaResultWrapper {
	private Integer i;

	public void addToInteger(Integer i) {
		this.i = this.i == null ? i : this.i + i;
	}

	public Integer getInteger() {
		return i;
	}
}
