package de.jottyfan.donationrunner.modules.help;

import java.io.Serializable;

/**
 *
 * @author jotty
 *
 */
public class Euro implements Serializable {
	private static final long serialVersionUID = 1L;

	private Double euro;

	private Euro() {
		this.euro = Double.valueOf(0);
	}

	@Override
	public String toString() {
		return euro == null ? "0 €" : String.format("%.2f €", euro);
	}

	public static final Euro of(Double d) {
		Euro euro = new Euro();
		euro.setEuro(d);
		return euro;
	}

	protected void setEuro(Double d) {
		this.euro = d;
	}
}
