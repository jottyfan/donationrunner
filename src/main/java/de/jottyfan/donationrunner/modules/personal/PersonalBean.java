package de.jottyfan.donationrunner.modules.personal;

/**
 * 
 * @author jotty
 *
 */
public class PersonalBean {
	private final String forename;
	private final String surname;
	private final String company;
	private final String street;
	private final String zip;
	private final String city;
	private final String email;
	private final String phone;
	private final String km;
	private final String donationgoal;
	private final String euro;

	public PersonalBean(String forename, String surname, String company, String street, String zip, String city, String email, String phone, String km, String donationgoal, String euro) {
		super();
		this.forename = forename;
		this.surname = surname;
		this.company = company;
		this.street = street;
		this.zip = zip;
		this.city = city;
		this.email = email;
		this.phone = phone;
		this.km = km;
		this.donationgoal = donationgoal;
		this.euro = euro;
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @return the km
	 */
	public String getKm() {
		return km;
	}

	/**
	 * @return the euro
	 */
	public String getEuro() {
		return euro;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @return the donationgoal
	 */
	public String getDonationgoal() {
		return donationgoal;
	}
}
