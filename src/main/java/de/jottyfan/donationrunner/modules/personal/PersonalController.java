package de.jottyfan.donationrunner.modules.personal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author jotty
 *
 */
@Controller
public class PersonalController {
	@Autowired
	private HttpServletRequest request;

	@Autowired
	private PersonalService service;

	@PostMapping("/personal")
	public String toPersonal(final Model model, RedirectAttributes redirect) {
		try {
			String secret = request.getParameter("secret");
			model.addAttribute("donators", service.getDonators(secret));
			model.addAttribute("runners", service.getRunners(secret));
			return "/personal";
		} catch (Exception e) {
			redirect.addFlashAttribute("error", e.getMessage());
			return "redirect:/";
		}
	}
}
