package de.jottyfan.donationrunner.modules.personal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jotty
 *
 */
@Service
public class PersonalService {

	@Autowired
	private PersonalRepository repository;

	public List<PersonalBean> getRunners(String secret) {
		return repository.getRunners(secret);
	}

	public List<PersonalBean> getDonators(String secret) {
		return repository.getDonators(secret);
	}
}
