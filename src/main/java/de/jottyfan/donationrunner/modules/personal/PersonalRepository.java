package de.jottyfan.donationrunner.modules.personal;

import static de.jottyfan.donationrunner.db.jooq.Tables.T_DONATIONGOAL;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_KILOMETER;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_PROPERTY;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_RUNNER;
import static de.jottyfan.donationrunner.db.jooq.Tables.V_DONATION;
import static de.jottyfan.donationrunner.db.jooq.Tables.V_DONATOR;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record10;
import org.jooq.SelectHavingStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.donationrunner.modules.help.Euro;

/**
 *
 * @author jotty
 *
 */
@Repository
public class PersonalRepository  {
	private static final Logger LOGGER = LogManager.getLogger(PersonalRepository.class);

	@Autowired
	private DSLContext jooq;

	private boolean checkSecret(String secret) {
		return jooq.fetchCount(jooq
		// @formatter:off
			.select(T_PROPERTY.ID)
			.from(T_PROPERTY)
			.where(T_PROPERTY.KEY.eq("password"))
			.and(T_PROPERTY.VALUE.eq(secret))) > 0;
		// @formatter:on
		// do not log this as secret is a password that should not appear in log files
	}

	/**
	 * get detailed summary for the privileged users
	 *
	 * @return the list of runners and their km and donation total
	 */
	public List<PersonalBean> getRunners(String secret) {
		if (!checkSecret(secret)) {
			throw new DataAccessException("Das eingegebene Passwort ist falsch.");
		}
		List<PersonalBean> list = new ArrayList<>();
		SelectOnConditionStep<Record10<String, String, String, String, String, String, String, BigDecimal, String, Double>> sql = jooq
		// @formatter:off
			.select(T_RUNNER.FORENAME,
					    T_RUNNER.SURNAME,
					    T_RUNNER.STREET,
					    T_RUNNER.ZIP,
					    T_RUNNER.CITY,
					    T_RUNNER.MAIL,
					    T_RUNNER.PHONE,
					    T_KILOMETER.KILOMETER,
					    T_DONATIONGOAL.NAME,
					    V_DONATION.DONATION_TOTAL)
			.from(T_RUNNER)
			.leftJoin(T_KILOMETER).on(T_KILOMETER.ID.eq(T_RUNNER.FK_KILOMETER))
			.leftJoin(T_DONATIONGOAL).on(T_DONATIONGOAL.ID.eq(T_RUNNER.FK_DONATIONGOAL))
			.leftJoin(V_DONATION).on(V_DONATION.ALIAS.eq(T_RUNNER.ALIAS));
		// @formatter:on
		LOGGER.debug(sql.toString());
		for (Record r : sql.fetch()) {
			String forename = r.get(T_RUNNER.FORENAME);
			String surname = r.get(T_RUNNER.SURNAME);
			String street = r.get(T_RUNNER.STREET);
			String zip = r.get(T_RUNNER.ZIP);
			String city = r.get(T_RUNNER.CITY);
			String mail = r.get(T_RUNNER.MAIL);
			String phone = r.get(T_RUNNER.PHONE);
			BigDecimal kilometer = r.get(T_KILOMETER.KILOMETER);
			String donationgoal = r.get(T_DONATIONGOAL.NAME);
			Double total = r.get(V_DONATION.DONATION_TOTAL);
			Euro euro = Euro.of(total);
			String km = kilometer == null ? "0" : kilometer.toString();
			list.add(new PersonalBean(forename, surname, null, street, zip, city, mail, phone, km, donationgoal, euro.toString()));
		}
		return list;
	}

	/**
	 * get list of donators
	 *
	 * @param secret
	 *          the secret
	 * @return list of donators if secret is valid
	 */
	public List<PersonalBean> getDonators(String secret) {
		if (!checkSecret(secret)) {
			throw new DataAccessException("Das eingegebene Passwort ist falsch.");
		}
		List<PersonalBean> list = new ArrayList<>();
		SelectHavingStep<Record10<String, String, String, String, String, String, String, String, Long, Double>> sql = jooq
		// @formatter:off
			.select(V_DONATOR.FORENAME,
					    V_DONATOR.SURNAME,
					    V_DONATOR.COMPANY,
					    V_DONATOR.STREET,
					    V_DONATOR.ZIP,
					    V_DONATOR.CITY,
					    V_DONATOR.MAIL,
					    V_DONATOR.PHONE,
					    V_DONATOR.RUNNERS,
					    V_DONATOR.EURO)
			.from(V_DONATOR);
		// @formatter:on
		LOGGER.debug(sql.toString());
		for (Record r : sql.fetch()) {
			String forename = r.get(V_DONATOR.FORENAME);
			String surname = r.get(V_DONATOR.SURNAME);
			String company = r.get(V_DONATOR.COMPANY);
			String street = r.get(V_DONATOR.STREET);
			String zip = r.get(V_DONATOR.ZIP);
			String city = r.get(V_DONATOR.CITY);
			String mail = r.get(V_DONATOR.MAIL);
			String phone = r.get(V_DONATOR.PHONE);
			Long l = r.get(V_DONATOR.RUNNERS);
			String runners = l == null ? "" : l.toString();
			Double total = r.get(V_DONATOR.EURO);
			Euro euro = Euro.of(total);
			list.add(new PersonalBean(forename, surname, company, street, zip, city, mail, phone, runners, null, euro.toString()));
		}
		return list;
	}

}
