package de.jottyfan.donationrunner.modules.donator;

import java.io.Serializable;

import de.jottyfan.donationrunner.modules.help.Euro;

/**
 *
 * @author jotty
 *
 */
public class DonatorsumBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Euro euro;
	private Double kilometers;
	private Integer runners;

	public DonatorsumBean() {
		super();
		this.euro = Euro.of(0d);
		this.kilometers = 0d;
		this.runners = 0;
	}

	/**
	 * @return the euro
	 */
	public Euro getEuro() {
		return euro;
	}

	/**
	 * @param euro
	 *          the euro to set
	 */
	public void setEuro(Euro euro) {
		this.euro = euro;
	}

	/**
	 * @return the kilometers
	 */
	public Double getKilometers() {
		return kilometers;
	}

	/**
	 * @param kilometers
	 *          the kilometers to set
	 */
	public void setKilometers(Double kilometers) {
		this.kilometers = kilometers;
	}

	/**
	 * @return the runners
	 */
	public Integer getRunners() {
		return runners;
	}

	/**
	 * @param runners
	 *          the runners to set
	 */
	public void setRunners(Integer runners) {
		this.runners = runners;
	}
}
