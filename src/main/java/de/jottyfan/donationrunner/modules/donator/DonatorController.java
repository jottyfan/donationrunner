package de.jottyfan.donationrunner.modules.donator;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author jotty
 *
 */
@Controller
public class DonatorController {

	@Autowired
	private DonatorService service;

	@GetMapping("/donator")
	public String toDonator(final Model model) {
		model.addAttribute("bean", service.getDonator());
		return "/donator";
	}

	@PostMapping("/donator/register")
	public String doRegister(@ModelAttribute("bean") @Valid DonatorBean bean, BindingResult result, final Model model, RedirectAttributes redirect) {
		if (result.hasErrors()) {
			bean.refresh(service.getRunnerList());
			return "/donator";
		}
		String message = service.doRegister(bean);
		redirect.addFlashAttribute("message", message);
		return "redirect:/";
	}
}
