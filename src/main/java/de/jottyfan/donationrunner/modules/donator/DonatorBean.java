package de.jottyfan.donationrunner.modules.donator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import de.jottyfan.donationrunner.modules.help.Euro;

/**
 *
 * @author jotty
 *
 */
public class DonatorBean implements Serializable {
	private static final long serialVersionUID = 2L;

	@NotBlank
	private String forename;
	@NotBlank
	private String surname;
	private String company;
	@NotBlank
	private String street;
	@NotBlank
	@Size(min = 5, max = 5)
	private String zip;
	@NotBlank
	private String city;
	@Email
	private String mail;
	@Pattern(regexp = "^\\d*\\/?\\d+$", message = "Das Format muss Vorwahl/Rufnummer oder nur Rufnummer sein. Leerzeichen sind nicht erlaubt.")
	private String phone;

	private List<DonationBean> donations;

	public DonatorBean() {
		donations = new ArrayList<>();
	}

	public void refresh(List<DonationBean> runnerList) {
		donations.clear();
		donations.addAll(runnerList);
	}

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<DonationBean> getDonations() {
		return donations;
	}
}
