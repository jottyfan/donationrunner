package de.jottyfan.donationrunner.modules.donator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jotty
 *
 */
@Service
public class DonatorService {

	@Autowired
	private DonatorRepository repository;

	public DonatorBean getDonator() {
		DonatorBean bean = new DonatorBean();
		bean.getDonations().addAll(repository.getRunnerList());
		return bean;
	}

	public String doRegister(DonatorBean bean) {
		Integer donatorId = repository.addDonations(bean);
		DonatorsumBean sum = repository.getDonationSummary(donatorId);
		StringBuilder buf = new StringBuilder("Vielen Dank für Deine Spende!<br />");
		buf.append("Damit hilfst Du uns, Kindern und Jugendlichen einen Ort zu geben, an dem sie sich mit ihren Potentialen entfalten können.<br />");
		buf.append("Die Spendenzusage ist eingegangen und beträgt ");
		buf.append(sum.getEuro().toString()).append(" für insgesamt ");
		buf.append(sum.getKilometers()).append(" km von ");
		buf.append(sum.getRunners()).append(" Läufer_innen.<br />");
		buf.append("Du erhältst von uns nach dem Oberelbemarathon eine Spendenerinnerung. Bei Spenden über 100€ erhältst du eine Spendenquittung im Anschluss.");
		return buf.toString();
	}

	public List<DonationBean> getRunnerList() {
		return repository.getRunnerList();
	}
}
