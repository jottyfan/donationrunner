package de.jottyfan.donationrunner.modules.donator;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.format.annotation.NumberFormat;

/**
 *
 * @author jotty
 *
 */
public class DonationBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private final String alias;
	private final Double kilometer;

	private Double donation;

	public DonationBean() {
		this.id = null;
		this.alias = null;
		this.kilometer = null;
	}

	public DonationBean(Integer id, String alias, Double kilometer) {
		this.id = id;
		this.alias = alias;
		this.kilometer = kilometer;
	}

	public String getDivid() {
		return "runner" + id;
	}

	public Integer getId() {
		return id;
	}

	public String getAlias() {
		return alias;
	}

	public Double getKilometer() {
		return kilometer;
	}

	public Double getDonation() {
		return donation;
	}

	public void setDonation(Double euro) {
		this.donation = euro;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
}
