package de.jottyfan.donationrunner.modules.donator;

import static de.jottyfan.donationrunner.db.jooq.Tables.T_DONATION;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_DONATOR;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_KILOMETER;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_RUNNER;
import static de.jottyfan.donationrunner.db.jooq.Tables.V_DONATORSUM;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.InsertResultStep;
import org.jooq.InsertValuesStep3;
import org.jooq.Record;
import org.jooq.Record3;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.donationrunner.db.LambdaResultWrapper;
import de.jottyfan.donationrunner.db.jooq.tables.records.TDonationRecord;
import de.jottyfan.donationrunner.db.jooq.tables.records.TDonatorRecord;
import de.jottyfan.donationrunner.modules.help.Euro;

/**
 *
 * @author jotty
 *
 */
@Repository
public class DonatorRepository {
	private final static Logger LOGGER = LogManager.getLogger(DonatorRepository.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * load all runners
	 *
	 * @return list of runners, at least an empty one
	 */
	public List<DonationBean> getRunnerList() {
		SelectJoinStep<Record3<Integer, String, BigDecimal>> sql = jooq
		// @formatter:off
			.select(T_RUNNER.ID,
					    T_RUNNER.ALIAS,
					    T_KILOMETER.KILOMETER)
			.from(T_RUNNER)
			.leftJoin(T_KILOMETER).on(T_KILOMETER.ID.eq(T_RUNNER.FK_KILOMETER));
		// @formatter:off
		LOGGER.debug(sql.toString());
		List<DonationBean> list = new ArrayList<>();
		for (Record r : sql.fetch()) {
			Integer id = r.get(T_RUNNER.ID);
			String alias = r.get(T_RUNNER.ALIAS);
			BigDecimal kilometer = r.get(T_KILOMETER.KILOMETER);
			list.add(new DonationBean(id, alias, kilometer == null ? null : kilometer.doubleValue()));
		}
		return list;
	}

	/**
	 * add donations to db
	 *
	 * @param bean the donations information
	 * @return the new ID of the donator
	 */
	public Integer addDonations(DonatorBean bean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		jooq.transaction(t->{
			InsertResultStep<TDonatorRecord> sql = DSL.using(t)
			// @formatter:off
				.insertInto(T_DONATOR,
						        T_DONATOR.FORENAME,
						        T_DONATOR.SURNAME,
						        T_DONATOR.COMPANY,
						        T_DONATOR.STREET,
						        T_DONATOR.ZIP,
						        T_DONATOR.CITY,
						        T_DONATOR.MAIL,
						        T_DONATOR.PHONE)
				.values(bean.getForename(),
						    bean.getSurname(),
						    bean.getCompany(),
						    bean.getStreet(),
						    bean.getZip(),
						    bean.getCity(),
						    bean.getMail(),
						    bean.getPhone())
				.returning(T_DONATOR.ID);
			// @formatter:on
			LOGGER.debug(sql.toString());
			Integer donatorId = sql.fetchOne().getId();
			lrw.addToInteger(donatorId);

			for (DonationBean b : bean.getDonations()) {
				if (b.getDonation() != null) {
					InsertValuesStep3<TDonationRecord, Integer, Integer, Double> sql2 = DSL.using(t)
					// @formatter:off
						.insertInto(T_DONATION,
												T_DONATION.FK_DONATOR,
												T_DONATION.FK_RUNNER,
												T_DONATION.KMDONATION)
						.values(donatorId, b.getId(), b.getDonation());
					// @formatter:on
					LOGGER.debug(sql2.toString());
					sql2.execute();
				}
			}
		});
		return lrw.getInteger();
	}

	public DonatorsumBean getDonationSummary(Integer donatorId) {
		SelectConditionStep<Record3<Double, BigDecimal, Long>> sql = jooq
		// @formatter:off
		  .select(V_DONATORSUM.EURO,
		  		    V_DONATORSUM.KILOMETER,
		  		    V_DONATORSUM.RUNNERS)
		  .from(V_DONATORSUM)
		  .where(V_DONATORSUM.FK_DONATOR.eq(donatorId));
		// @formatter:on
		LOGGER.debug(sql.toString());
		Record3<Double, BigDecimal, Long> r = sql.fetchOne();
		DonatorsumBean bean = new DonatorsumBean();
		if (r != null) {
			BigDecimal kilometer = r.get(V_DONATORSUM.KILOMETER);
			Long runners = r.get(V_DONATORSUM.RUNNERS);
			bean.setEuro(Euro.of(r.get(V_DONATORSUM.EURO)));
			bean.setKilometers(kilometer == null ? Double.valueOf(0) : kilometer.doubleValue());
			bean.setRunners(runners == null ? 0 : runners.intValue());
		}
		return bean;
	}
}
