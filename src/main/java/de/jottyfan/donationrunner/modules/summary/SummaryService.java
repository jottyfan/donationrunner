package de.jottyfan.donationrunner.modules.summary;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jotty
 *
 */
@Service
public class SummaryService {

	@Autowired
	private SummaryRepository gateway;

	public String getSummary() {
		return gateway.getPublicSummary();
	}

	public List<SummaryBean> getList() {
		return gateway.getList();
	}
}
