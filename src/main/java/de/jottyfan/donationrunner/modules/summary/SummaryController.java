package de.jottyfan.donationrunner.modules.summary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author jotty
 *
 */
@Controller
public class SummaryController {

	@Autowired
	private SummaryService service;

	@GetMapping("/summary")
	public String toSummary(final Model model) {
		model.addAttribute("publicSummary", service.getSummary());
		model.addAttribute("list", service.getList());
		return "/summary";
	}
}
