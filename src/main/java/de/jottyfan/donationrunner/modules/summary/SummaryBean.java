package de.jottyfan.donationrunner.modules.summary;

import java.math.BigDecimal;

import de.jottyfan.donationrunner.modules.help.Euro;

/**
 *
 * @author jotty
 *
 */
public class SummaryBean {
	private final String alias;
	private final BigDecimal km;
	private final Euro euro;

	public SummaryBean(String alias, BigDecimal km, Euro euro) {
		super();
		this.alias = alias;
		this.km = km;
		this.euro = euro;
	}

	public String getAlias() {
		return alias;
	}

	public BigDecimal getKm() {
		return km;
	}

	public Euro getEuro() {
		return euro;
	}
}
