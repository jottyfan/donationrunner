package de.jottyfan.donationrunner.modules.summary;

import static de.jottyfan.donationrunner.db.jooq.Tables.T_KILOMETER;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_RUNNER;
import static de.jottyfan.donationrunner.db.jooq.Tables.V_DONATION;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.SelectJoinStep;
import org.jooq.SelectOnConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.donationrunner.modules.help.Euro;

/**
 *
 * @author jotty
 *
 */
@Repository
public class SummaryRepository {
	private static final Logger LOGGER = LogManager.getLogger(SummaryRepository.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * get global summary for the world
	 *
	 * @return a summary string
	 */
	public String getPublicSummary() {
		SelectJoinStep<Record2<BigDecimal, Double>> sql = jooq
		// @formatter:off
			.select(V_DONATION.KILOMETER,
					    V_DONATION.DONATION_TOTAL)
			.from(V_DONATION);
		// @formatter:on
		LOGGER.debug(sql.toString());
		Integer x = 0;
		BigDecimal km = new BigDecimal(0);
		Double don = Double.valueOf(0);
		for (Record r : sql.fetch()) {
			x++;
			km = km.add(r.get(V_DONATION.KILOMETER));
			Double donationTotal = r.get(V_DONATION.DONATION_TOTAL);
			if (donationTotal != null) {
				don += donationTotal;
			}
		}
		StringBuilder buf = new StringBuilder("Es haben sich bisher ");
		buf.append(x).append(" Läufer_innen für insgesamt ");
		buf.append(km).append(" km registriert. Das spielt eine Spendensumme von ");
		buf.append(Euro.of(don).toString()).append(" ein.");
		return buf.toString();
	}

	/**
	 * get list of all runners and their donation sum
	 *
	 * @return list of runners (alias, km and donation sum only)
	 */
	public List<SummaryBean> getList() {
		List<SummaryBean> list = new ArrayList<>();
		SelectOnConditionStep<Record3<String, BigDecimal, Double>> sql = jooq
		// @formatter:off
			.select(T_RUNNER.ALIAS,
					    T_KILOMETER.KILOMETER,
					    V_DONATION.DONATION_TOTAL)
			.from(T_RUNNER)
			.leftJoin(T_KILOMETER).on(T_KILOMETER.ID.eq(T_RUNNER.FK_KILOMETER))
			.leftJoin(V_DONATION).on(V_DONATION.ALIAS.eq(T_RUNNER.ALIAS));
		// @formatter:on
		LOGGER.debug(sql.toString());
		for (Record r : sql.fetch()) {
			String alias = r.get(T_RUNNER.ALIAS);
			BigDecimal km = r.get(T_KILOMETER.KILOMETER);
			Double total = r.get(V_DONATION.DONATION_TOTAL);
			Euro euro = Euro.of(total);
			list.add(new SummaryBean(alias, km, euro));
		}
		return list;
	}
}
