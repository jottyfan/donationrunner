package de.jottyfan.donationrunner.modules;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author jotty
 *
 */
@Controller
public class IndexController {

	@GetMapping("/")
	public String getIndex() {
		return "/index";
	}

}
