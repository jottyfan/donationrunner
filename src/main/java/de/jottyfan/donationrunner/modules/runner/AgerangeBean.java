package de.jottyfan.donationrunner.modules.runner;

import java.io.Serializable;

/**
 * 
 * @author jotty
 *
 */
public class AgerangeBean implements Serializable, Comparable<AgerangeBean> {
	private static final long serialVersionUID = 1L;

	private final Integer id;
	private final String name;

	public AgerangeBean(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int compareTo(AgerangeBean bean) {
		return bean == null || id == null || bean.getId() == null ? 0 : id.compareTo(bean.getId());
	}
}
