package de.jottyfan.donationrunner.modules.runner;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import de.jottyfan.donationrunner.modules.runner.validator.UnusedAlias;

/**
 *
 * @author jotty
 *
 */
@UnusedAlias(field = "alias", message = "Dieses Alias ist leider bereits vergeben. Bitte wähle ein anderes.")
public class RunnerBean {

	@NotBlank
	private String alias;
	@NotBlank
	private String forename;
	@NotBlank
	private String surname;
	@NotBlank
	private String street;
	@NotBlank
	@Size(min = 5, max = 5)
	private String zip;
	@NotBlank
	private String city;
	@NotBlank
	@Email
	private String mail;
	@NotBlank
	@Pattern(regexp = "^\\d*\\/?\\d+$", message = "Das Format muss Vorwahl/Rufnummer oder nur Rufnummer sein. Leerzeichen sind nicht erlaubt.")
	private String phone;

	private Integer kilometer;
	private Integer agerange;
	private Integer donationgoal;

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getKilometer() {
		return kilometer;
	}

	public void setKilometer(Integer kilometer) {
		this.kilometer = kilometer;
	}

	public Integer getAgerange() {
		return agerange;
	}

	public void setAgerange(Integer agerange) {
		this.agerange = agerange;
	}

	public Integer getDonationgoal() {
		return donationgoal;
	}

	public void setDonationgoal(Integer donationgoal) {
		this.donationgoal = donationgoal;
	}
}
