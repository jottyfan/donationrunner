package de.jottyfan.donationrunner.modules.runner;

/**
 * 
 * @author jotty
 *
 */
public class DonationgoalBean {
	private final Integer id;
	private final String name;

	public DonationgoalBean(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
