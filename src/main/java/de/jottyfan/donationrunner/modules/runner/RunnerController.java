package de.jottyfan.donationrunner.modules.runner;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author jotty
 *
 */
@Controller
public class RunnerController {

	@Autowired
	private RunnerModel service;

	@GetMapping("/runner")
	public String getRunner(final Model model) {
		model.addAttribute("ageranges", service.getAgeranges());
		model.addAttribute("kilometers", service.getKilometers());
		model.addAttribute("donationgoals", service.getDonationgoals());
		model.addAttribute("bean", new RunnerBean());
		return "/runner";
	}

	@PostMapping("/runner/register")
	public String doRegister(@ModelAttribute("bean") @Valid RunnerBean bean, BindingResult result, final Model model, RedirectAttributes redirect) {
		if (result.hasErrors()) {
			model.addAttribute("ageranges", service.getAgeranges());
			model.addAttribute("kilometers", service.getKilometers());
			model.addAttribute("donationgoals", service.getDonationgoals());
			return "/runner";
		}
		String message = service.doRegister(bean, service.getKilometers());
		redirect.addFlashAttribute("message", message);
		return "redirect:/";
	}
}
