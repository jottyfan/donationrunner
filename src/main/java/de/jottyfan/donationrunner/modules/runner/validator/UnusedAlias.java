package de.jottyfan.donationrunner.modules.runner.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author jotty
 *
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UnusedAliasValidator.class)
@Documented
public @interface UnusedAlias {
	String message() default "alias is already in use";

	String field();

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
