package de.jottyfan.donationrunner.modules.runner.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;

import de.jottyfan.donationrunner.modules.runner.RunnerRepository;

/**
 *
 * @author jotty
 *
 */
public class UnusedAliasValidator implements ConstraintValidator<UnusedAlias, Object> {

	private String field;
	private String message;

	@Autowired
	private RunnerRepository repository;

	public void initialize(UnusedAlias uu) {
		this.field = uu.field();
		this.message = uu.message();
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		Object alias = new BeanWrapperImpl(value).getPropertyValue(field);
		Boolean result = repository.aliasNotYetFound((String) alias);
		if (!result) {
			context.buildConstraintViolationWithTemplate(message).addPropertyNode(field).addConstraintViolation()
					.disableDefaultConstraintViolation();
		}
		return result;
	}

}
