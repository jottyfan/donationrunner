package de.jottyfan.donationrunner.modules.runner;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author jotty
 *
 */
public class KilometerBean implements Serializable, Comparable<KilometerBean> {
	private static final long serialVersionUID = 1L;

	private final Integer id;
	private final String name;
	private final BigDecimal kilometer;

	public KilometerBean(Integer id, String name, BigDecimal kilometer) {
		super();
		this.id = id;
		this.name = name;
		this.kilometer = kilometer;
	}

	@Override
	public int compareTo(KilometerBean o) {
		return kilometer == null || o == null || o.getKilometer() == null ? 0 : o.getKilometer().compareTo(kilometer);
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getKilometer() {
		return kilometer;
	}
}
