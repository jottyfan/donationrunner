package de.jottyfan.donationrunner.modules.runner;

import static de.jottyfan.donationrunner.db.jooq.Tables.T_AGERANGE;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_DONATIONGOAL;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_KILOMETER;
import static de.jottyfan.donationrunner.db.jooq.Tables.T_RUNNER;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.InsertValuesStep11;
import org.jooq.SelectConditionStep;
import org.jooq.SelectWhereStep;
import org.jooq.exception.TooManyRowsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.donationrunner.db.jooq.tables.records.TAgerangeRecord;
import de.jottyfan.donationrunner.db.jooq.tables.records.TDonationgoalRecord;
import de.jottyfan.donationrunner.db.jooq.tables.records.TKilometerRecord;
import de.jottyfan.donationrunner.db.jooq.tables.records.TRunnerRecord;

/**
 *
 * @author jotty
 *
 */
@Repository
public class RunnerRepository {
	private static final Logger LOGGER = LogManager.getLogger(RunnerRepository.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * register the runner
	 *
	 * @param bean
	 *          the data of the runner
	 * @return the number of affected database rows, should be 1
	 */
	public Integer doRegister(RunnerBean bean) {
		InsertValuesStep11<TRunnerRecord, String, String, String, String, String, String, String, String, Integer, Integer, Integer> sql = jooq
		// @formatter:off
			.insertInto(T_RUNNER,
				          T_RUNNER.ALIAS,
				          T_RUNNER.FORENAME,
				          T_RUNNER.SURNAME,
				          T_RUNNER.STREET,
				          T_RUNNER.ZIP,
				          T_RUNNER.CITY,
				          T_RUNNER.MAIL,
				          T_RUNNER.PHONE,
				          T_RUNNER.FK_KILOMETER,
				          T_RUNNER.FK_AGERANGE,
				          T_RUNNER.FK_DONATIONGOAL)
			.values(bean.getAlias(), bean.getForename(), bean.getSurname(), bean.getStreet(), bean.getZip(),
					    bean.getCity(), bean.getMail(), bean.getPhone(), bean.getKilometer(), bean.getAgerange(), bean.getDonationgoal());
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	/**
	 * get all age ranges from db
	 *
	 * @return list of ageranges, sorted by id
	 */
	public List<AgerangeBean> getAgeranges() {
		SelectWhereStep<TAgerangeRecord> sql = jooq.selectFrom(T_AGERANGE);
		LOGGER.debug(sql.toString());
		List<AgerangeBean> list = new ArrayList<>();
		for (TAgerangeRecord r : sql.fetch()) {
			list.add(new AgerangeBean(r.getId(), r.getName()));
		}
		list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return list;
	}

	/**
	 * get list of valid kilometers
	 *
	 * @return list of kilometers, sorted by distance
	 */
	public List<KilometerBean> getKilometers() {
		SelectWhereStep<TKilometerRecord> sql = jooq.selectFrom(T_KILOMETER);
		LOGGER.debug(sql.toString());
		List<KilometerBean> list = new ArrayList<>();
		for (TKilometerRecord r : sql.fetch()) {
			list.add(new KilometerBean(r.getId(), r.getName(), r.getKilometer()));
		}
		list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return list;
	}

	/**
	 * get list of donation goals
	 *
	 * @return list of donation goals, at least an empty one
	 */
	public List<DonationgoalBean> getDonationgoals() {
		SelectWhereStep<TDonationgoalRecord> sql = jooq.selectFrom(T_DONATIONGOAL);
		LOGGER.debug(sql.toString());
		List<DonationgoalBean> list = new ArrayList<>();
		for (TDonationgoalRecord r : sql.fetch()) {
			list.add(new DonationgoalBean(r.getId(), r.getName()));
		}
		return list;
	}

	/**
	 * check if the alias has not been used yet
	 *
	 * @param alias
	 *          the alias
	 * @return true or false
	 */
	public Boolean aliasNotYetFound(String alias) {
		SelectConditionStep<TRunnerRecord> sql = jooq.selectFrom(T_RUNNER).where(T_RUNNER.ALIAS.eq(alias));
		LOGGER.debug(sql.toString());
		try {
			return sql.fetchOne() == null;
		} catch (TooManyRowsException e) {
			return false; // multiple entries are forbidden by the database by a unique constraint, but who knows...
		}
	}
}
