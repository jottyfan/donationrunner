package de.jottyfan.donationrunner.modules.runner;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jotty
 *
 */
@Service
public class RunnerModel {

	@Autowired
	private RunnerRepository repository;

	public List<AgerangeBean> getAgeranges() {
		return repository.getAgeranges();
	}

	public List<KilometerBean> getKilometers() {
		return repository.getKilometers();
	}

	public List<DonationgoalBean> getDonationgoals() {
		return repository.getDonationgoals();
	}

	public String doRegister(RunnerBean bean, List<KilometerBean> kmList) {
		repository.doRegister(bean);
		BigDecimal km = new BigDecimal(0);
		for (KilometerBean kmBean : kmList) {
			if (kmBean.getId().equals(bean.getKilometer())) {
				km = kmBean.getKilometer();
			}
		}
		StringBuilder buf = new StringBuilder(bean.getAlias());
		buf.append(" hat sich für das Erlaufen von ").append(km);
		buf.append(" km verpflichtet.");
		return buf.toString();
	}
}
